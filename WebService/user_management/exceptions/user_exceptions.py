from rest_framework import status
from rest_framework.exceptions import APIException


class InvalidUsername(APIException):
    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = {'username': 'User or password is incorrect, please check again'}
    default_code = 'credentials'


class InvalidPassword(APIException):
    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = {'password': 'User or password is incorrect, please check again'}
    default_code = 'credentials'


class InvalidUserId(APIException):
    status_code = status.HTTP_401_UNAUTHORIZED
    default_detail = 'The provided ID does not belong to any user.'
    default_code = 701
