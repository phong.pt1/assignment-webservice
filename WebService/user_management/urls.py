from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import (
    TokenBlacklistView, TokenObtainPairView, TokenRefreshView, TokenVerifyView
)

from .views import UserViewSet, RoleViewSet, PermissionVerifyView, RoleVerifyView

router = DefaultRouter()
router.register(r'users', UserViewSet, basename='users')
router.register(r'roles', RoleViewSet, basename='roles')

urlpatterns = [
    path("login/", TokenObtainPairView.as_view()),
    path("logout/", TokenBlacklistView.as_view()),
    path("token/refresh/", TokenRefreshView.as_view()),
    path("token/verify/", TokenVerifyView.as_view()),
    path("role/verify/", RoleVerifyView.as_view()),
    path("permission/verify/", PermissionVerifyView.as_view()),
]

urlpatterns += router.urls
