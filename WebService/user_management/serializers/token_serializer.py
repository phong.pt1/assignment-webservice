from django.conf import settings
from django.contrib.auth.models import update_last_login
from rest_framework import serializers, exceptions
from rest_framework_simplejwt.serializers import (
    TokenBlacklistSerializer, TokenObtainPairSerializer, TokenRefreshSerializer, TokenVerifySerializer
)
from rest_framework_simplejwt.token_blacklist.models import BlacklistedToken, OutstandingToken
from rest_framework_simplejwt.tokens import UntypedToken
from rest_framework_simplejwt.utils import datetime_from_epoch

from ..exceptions import InvalidUserId, InvalidUsername, InvalidPassword
from ..models import User
from ..helpers.authentication import InvalidMessage


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    def validate(self, attrs):
        u = User.objects.filter(username=attrs.get('username')).first()
        if not u:
            raise InvalidUsername
        if not u.check_password(attrs.get('password')):
            raise InvalidPassword
        if not u.status:
            raise exceptions.AuthenticationFailed(InvalidMessage.USER_INACTIVE)

        data = super().validate(attrs)

        update_last_login(None, self.user)

        return {
            'access': data['access'],
            'refresh': data['refresh'],
            'user_id': self.user.id,
            'username': self.user.username,
            'email': self.user.email,
            'role': self.user.role.code,
            'permissions': self.user.get_permissions(),
        }

    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        token['role'] = user.role.code
        return token


class CustomTokenRefreshSerializer(TokenRefreshSerializer):
    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    def validate(self, attrs):
        refresh = self.token_class(attrs['refresh'])
        user = User.objects.filter(pk=refresh.payload.get('user_id')).select_related('role').first()
        if not user or not user.status:
            raise InvalidUserId

        return {
            'access': str(refresh.access_token),
            'refresh': attrs['refresh'],
            'user_id': user.id,
            'username': user.username,
            'email': user.email,
            'role': user.role.code,
            'permissions': user.get_permissions(),
        }


class CustomTokenBlacklistSerializer(TokenBlacklistSerializer):
    token = serializers.CharField()

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    def validate(self, attrs):
        # add access token to blacklist
        token = UntypedToken(attrs['token'])
        self.__add_token_to_blacklist(token)

        # add refresh to blacklist
        refresh = UntypedToken(attrs['refresh'])
        self.__add_token_to_blacklist(refresh)

        return {}

    @staticmethod
    def __add_token_to_blacklist(token):
        token, _ = OutstandingToken.objects.get_or_create(
            jti=token.get(settings.SIMPLE_JWT.get('JTI_CLAIM', 'jti')),
            defaults={
                'token': str(token),
                'user_id': token.get('user_id'),
                'expires_at': datetime_from_epoch(token.get('exp')),
            },
        )
        BlacklistedToken.objects.get_or_create(token=token)


class CustomTokenVerifySerializer(TokenVerifySerializer):
    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    def validate(self, attrs):
        token = UntypedToken(attrs['token'])
        jti = token.get(settings.SIMPLE_JWT.get('JTI_CLAIM', 'jti'))
        if BlacklistedToken.objects.filter(token__jti=jti).exists():
            raise exceptions.AuthenticationFailed(InvalidMessage.BLACK_LIST)

        user = User.objects.filter(pk=token.payload.get('user_id')).select_related('role').first()
        if not user or not user.status:
            raise InvalidUserId

        return {
            'user_id': user.id,
            'username': user.username,
            'role': user.role.code,
            'permissions': user.get_permissions(),
        }
