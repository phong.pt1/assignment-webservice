from rest_framework import serializers

from ..models import Role


class ListRoleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Role
        fields = ['id', 'name']
