from django.db import transaction
from django.core.validators import RegexValidator, EmailValidator
from django.contrib.auth.hashers import make_password
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from ..models import User, Role
from ..helpers.user_validator import InvalidMessage
# from ...review.models import ReviewCase
# from ...respond.models import Rule


class BaseUserSerializer(serializers.Serializer):
    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    full_name = serializers.CharField(
        required=False, allow_null=True, allow_blank=True, max_length=100
    )
    email = serializers.CharField(
        required=False, allow_null=True, allow_blank=True, max_length=100,
        validators=[EmailValidator(), ]
    )
    role_id = serializers.IntegerField(required=True)
    status = serializers.BooleanField(required=True)

    cases = serializers.JSONField(required=False)
    rules = serializers.JSONField(required=False)

    def to_representation(self, instance):
        return ListUserSerializer(instance).data

    # @staticmethod
    # def _create_user_case(case_ids, case_dict, user, created_by):
    #     user_cases = []
    #     for case in ReviewCase.objects.filter(id__in=case_ids):
    #         user_cases.append(UserCase(
    #             user=user,
    #             case=case,
    #             role=case_dict.get(case.id),
    #             created_by=created_by
    #         ))
    #     UserCase.objects.bulk_create(user_cases)

    # @staticmethod
    # def _create_user_rule(rule_ids, rule_dict, user, created_by):
    #     user_rules = []
    #     for rule in Rule.objects.filter(id__in=rule_ids):
    #         user_rules.append(UserRule(
    #             user=user,
    #             rule=rule,
    #             role=rule_dict.get(rule.id),
    #             created_by=created_by
    #         ))
    #     UserRule.objects.bulk_create(user_rules)


class CreateUserSerializer(BaseUserSerializer):
    username = serializers.CharField(
        required=True, max_length=50,
        validators=[
            RegexValidator(User.USERNAME_REGEX[0], message=User.USERNAME_REGEX[1]),
            UniqueValidator(queryset=User.objects.all(), message=InvalidMessage.USER_UNIQUE),
        ]
    )
    password = serializers.CharField(
        required=True, max_length=100, min_length=6,
        validators=[RegexValidator(User.PASSWORD_REGEX[0], message=User.PASSWORD_REGEX[1]), ]
    )

    def validate_role_id(self, value):
        if not Role.objects.filter(id=value).exists():
            raise serializers.ValidationError(InvalidMessage.NOT_FOUND.format('Role', value))
        return value

    @transaction.atomic
    def create(self, validated_data):
        current_user = self.context['request'].user

        # cases = validated_data.pop('cases', None)
        # rules = validated_data.pop('rules', None)
        validated_data['password'] = make_password(validated_data['password'])
        validated_data['created_by'] = current_user.id
        user = User.objects.create(**validated_data)

        # if cases:
        #     self.__create_cases(cases, user, current_user.id)
        # if rules:
        #     self.__create_rules(rules, user, current_user.id)

        return user

    # def __create_cases(self, cases, user, created_by):
    #     case_dict, case_ids = {}, []
    #     for case in cases:
    #         case_id = case.get('id')
    #         case_dict[case_id] = case.get('role', UserCase.PREVIEW)
    #         case_ids.append(case_id)
    #
    #     self._create_user_case(case_ids, case_dict, user, created_by)
    #
    # def __create_rules(self, rules, user, created_by):
    #     rule_dict, rule_ids = {}, []
    #     for rule in rules:
    #         rule_id = rule.get('id')
    #         rule_dict[rule_id] = rule.get('role', UserRule.PREVIEW)
    #         rule_ids.append(rule_id)
    #
    #     self._create_user_rule(rule_ids, rule_dict, user, created_by)


class UpdateUserSerializer(BaseUserSerializer):
    @transaction.atomic
    def update(self, instance, validated_data):
        role_id = validated_data.get('role_id')
        if role_id != instance.role.id:
            role = Role.objects.filter(id=role_id).first()
            if not role:
                raise serializers.ValidationError({'role_id': [InvalidMessage.NOT_FOUND.format('Role', role_id)]})
            instance.role = role
        if validated_data.get('full_name') != instance.full_name:
            instance.full_name = validated_data.get('full_name')
        if validated_data.get('email') != instance.email:
            instance.email = validated_data.get('email')
        if validated_data.get('status') != instance.status:
            instance.status = validated_data.get('status')

        current_user = self.context['request'].user
        instance.updated_by = current_user.id

        # cases = validated_data.get('cases')
        # if cases:
        #     self.__update_cases(cases, instance, current_user.id)
        #
        # rules = validated_data.get('rules')
        # if rules:
        #     self.__update_rules(rules, instance, current_user.id)

        instance.save()
        return instance

    # def __update_cases(self, cases, user, updated_by):
    #     case_dict, case_ids = {}, []
    #     for case in cases:
    #         case_id = case.get('id')
    #         case_dict[case_id] = case.get('role', UserCase.PREVIEW)
    #         case_ids.append(case_id)
    #
    #     # Delete old user_case
    #     UserCase.objects.filter(user=user).exclude(case_id__in=case_ids).delete()
    #
    #     # Update user_case
    #     for user_case in UserCase.objects.filter(user=user, case_id__in=case_ids):
    #         case_ids.remove(user_case.case.id)
    #         new_role = case_dict.get(user_case.case.id)
    #         if user_case.role == new_role:
    #             continue
    #         user_case.updated_by = updated_by
    #         user_case.role = new_role
    #         user_case.save()
    #
    #     # Create new user_case
    #     self._create_user_case(case_ids, case_dict, user, updated_by)
    #
    # def __update_rules(self, rules, user, updated_by):
    #     rule_dict, rule_ids = {}, []
    #     for rule in rules:
    #         rule_id = rule.get('id')
    #         rule_dict[rule_id] = rule.get('role', UserCase.PREVIEW)
    #         rule_ids.append(rule_id)
    #
    #     # Delete old user_rule
    #     UserRule.objects.filter(user=user).exclude(rule_id__in=rule_ids).delete()
    #
    #     # Update user_rule
    #     for user_rule in UserRule.objects.filter(user=user, rule_id__in=rule_ids):
    #         rule_ids.remove(user_rule.rule.id)
    #         new_role = rule_dict.get(user_rule.rule.id)
    #         if user_rule.role == new_role:
    #             continue
    #         user_rule.updated_by = updated_by
    #         user_rule.role = new_role
    #         user_rule.save()
    #
    #     # Create new user_rule
    #     self._create_user_rule(rule_ids, rule_dict, user, updated_by)


class ListUserSerializer(serializers.ModelSerializer):
    role = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ['id', 'username', 'full_name', 'email', 'role', 'created_at', 'status', ]

    def get_role(self, obj):
        role = obj.role
        return role.name if role else None


class DetailUserSerializer(serializers.ModelSerializer):
    role = serializers.SerializerMethodField()
    cases = serializers.SerializerMethodField()
    rules = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ['id', 'username', 'full_name', 'email', 'role', 'created_at', 'status', 'cases', 'rules', ]

    def get_role(self, obj):
        role = obj.role
        return {
            'id': role.id,
            'name': role.name,
            'code': role.code,
        } if role else None

    # def get_cases(self, obj):
    #     user_case_qs = UserCase.objects.filter(user=obj).select_related('case', 'case__created_by').values(
    #         'case__id', 'case__name', 'case__created_by__id', 'case__created_by__username',
    #         'case__created_by__full_name', 'created_at', 'role'
    #     )
    #     return [
    #         {
    #             'no': ind + 1,
    #             'id': case.get('case__id'),
    #             'name': case.get('case__name'),
    #             'owner': {
    #                 'id': case.get('case__created_by__id'),
    #                 'username': case.get('case__created_by__username'),
    #                 'full_name': case.get('case__created_by__full_name'),
    #             },
    #             'created_at': case.get('created_at'),
    #             'role': case.get('role'),
    #         } for ind, case in enumerate(user_case_qs)
    #     ]
    #
    # def get_rules(self, obj):
    #     user_rule_qs = UserRule.objects.filter(user=obj).select_related('rule', 'rule__created_by').values(
    #         'rule__id', 'rule__name', 'rule__created_by__id', 'rule__created_by__username',
    #         'rule__created_by__full_name', 'created_at', 'role'
    #     )
    #     return [
    #         {
    #             'no': ind + 1,
    #             'id': case.get('rule__id'),
    #             'name': case.get('rule__name'),
    #             'owner': {
    #                 'id': case.get('rule__created_by__id'),
    #                 'username': case.get('rule__created_by__username'),
    #                 'full_name': case.get('rule__created_by__full_name'),
    #             },
    #             'created_at': case.get('created_at'),
    #             'role': case.get('role'),
    #         } for ind, case in enumerate(user_rule_qs)
    #     ]


class ResetPasswordSerializer(serializers.Serializer):
    new_password = serializers.CharField(
        required=True, max_length=100, min_length=6,
        validators=[RegexValidator(User.PASSWORD_REGEX[0], message=User.PASSWORD_REGEX[1]), ]
    )

    def update(self, instance, validated_data):
        current_user = self.context['request'].user
        instance.updated_by = current_user.id
        instance.set_password(validated_data['new_password'])
        instance.save()
        return instance

    def create(self, validated_data):
        pass


class ChangePasswordSerializer(ResetPasswordSerializer):
    old_password = serializers.CharField(required=True, max_length=100)

    def update(self, instance, validated_data):
        if not instance.check_password(validated_data['old_password']):
            raise serializers.ValidationError({'old_password': [InvalidMessage.OLD_PASSWORD_MISMATCHED]})
        elif instance.check_password(validated_data['new_password']):
            raise serializers.ValidationError({'new_password': [InvalidMessage.NEW_PASSWORD_MATCHED]})

        current_user = self.context['request'].user
        instance.updated_by = current_user.id
        instance.set_password(validated_data['new_password'])
        instance.save()
        return instance

    def create(self, validated_data):
        pass


class DeleteUserSerializer(serializers.Serializer):
    users = serializers.ListField(child=serializers.IntegerField(), required=True)

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass
