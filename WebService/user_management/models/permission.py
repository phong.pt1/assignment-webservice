from django.db import models


class Permission(models.Model):
    code = models.CharField(max_length=150, unique=True)
    name = models.CharField(max_length=150)

    class Meta:
        db_table = 'permission'
