from django.db import models


class Role(models.Model):
    code = models.CharField(max_length=150, unique=True)
    name = models.CharField(max_length=150)
    permissions = models.ManyToManyField('Permission', related_name='role_set')

    objects = models.Manager()

    class Meta:
        db_table = 'role'
