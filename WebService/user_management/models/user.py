from django.contrib.auth.hashers import check_password, make_password
from django.contrib.auth.models import UserManager
from django.db import models

from common.base_model import BaseModel


class User(BaseModel):
    username = models.CharField(max_length=50, null=False, blank=False, unique=True)
    password = models.CharField(max_length=100, null=False, blank=False)
    full_name = models.CharField(max_length=100, null=True, blank=True)
    email = models.EmailField(max_length=100, null=True, blank=True)
    status = models.BooleanField(null=False, blank=False, default=True)
    last_login = models.DateTimeField(blank=True, null=True)

    role = models.ForeignKey('Role', null=True, related_name='user_set', on_delete=models.SET_NULL)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []
    USERNAME_REGEX = (r'^[a-zA-Z0-9]+$', 'This value does not match the required pattern - [a-zA-Z0-9]')
    PASSWORD_REGEX = (r'^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$', 'Password is not match the rule, please check again')

    objects = UserManager()

    class Meta:
        db_table = 'user'

    @property
    def is_anonymous(self):
        return False

    @property
    def is_authenticated(self):
        return self.status

    def check_password(self, raw_password):
        return check_password(raw_password, self.password)

    def set_password(self, raw_password):
        self.password = make_password(raw_password)

    def get_permissions(self):
        return self.role.permissions.all().values_list("code", flat=True)


def default_user_authentication_rule(user):
    return user is not None and user.status
