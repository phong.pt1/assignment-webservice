from django.core.management.base import BaseCommand

from ...models import Permission, Role
from ...helpers.permissions import ROLE_PERMISSION, ROLES


class Command(BaseCommand):
    help = 'Used to generate role & permission'

    def handle(self, *args, **options):
        try:
            print('=================SYNC PERMISSION=================')

            for role, permissions in ROLE_PERMISSION.items():
                perms, perm_ids = [], []
                for permission in permissions:
                    p, _ = Permission.objects.update_or_create(
                        code=permission,
                        defaults={
                            'name': permission.replace('_', ' '),
                        },
                    )
                    perms.append(p)
                    perm_ids.append(p.id)

                r, _ = Role.objects.update_or_create(
                    code=role,
                    defaults={
                        'name': ROLES.get(role, {}).get('name', role),
                    },
                )
                r.permissions.remove(*list(Permission.objects.exclude(id__in=perm_ids)))
                r.permissions.add(*perms)

            print('=================SUCCESS=================')
        except Exception as e:
            print(e)
            print('=================ERROR=================')
