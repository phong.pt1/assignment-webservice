from django.contrib.auth.hashers import make_password
from django.core.management.base import BaseCommand

from ...models import User, Role
from ...helpers.permissions import SYSTEM_ADMIN


class Command(BaseCommand):
    help = 'Used to create a System Admin'

    def handle(self, *args, **options):
        try:
            print('==============Create System Admin==============')

            username = input('Enter username: ')
            password = input('Enter password: ')
            email = input('Enter email (option): ')
            full_name = input('Enter full name (option): ')

            User.objects.create(
                username=username,
                password=make_password(password),
                email=email,
                full_name=full_name,
                role=Role.objects.filter(code=SYSTEM_ADMIN).first()
            )

            print('===================Success======================')
        except Exception as e:
            print(e)
            print('=================ERROR===================')
