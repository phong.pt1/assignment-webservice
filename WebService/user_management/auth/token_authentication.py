from django.conf import settings
from rest_framework import exceptions, authentication
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.token_blacklist.models import BlacklistedToken

from ..exceptions import InvalidUserId
from ..models import User
from ..helpers.authentication import InvalidMessage
from ..helpers.constants import TOKEN_KEYWORD, API_KEY_HEADER


class CustomJWTAuthentication(JWTAuthentication):
    token_keyword = TOKEN_KEYWORD
    api_key_header = API_KEY_HEADER

    def __get_token_from_header(self, request):
        auth = authentication.get_authorization_header(request)
        if not auth:
            return None

        auth = auth.split()
        if len(auth) == 1 or auth[0].lower() != self.token_keyword.lower().encode():
            raise exceptions.AuthenticationFailed(InvalidMessage.TOKEN_HEADER)
        elif len(auth) > 2:
            raise exceptions.AuthenticationFailed(InvalidMessage.TOKEN_HEADER_WITH_SPACE)

        return auth[1]

    def authenticate(self, request):
        raw_token = self.__get_token_from_header(request)
        if not raw_token:
            return None, None

        validated_token = self.get_validated_token(raw_token)

        jti = validated_token.get(settings.SIMPLE_JWT.get('JTI_CLAIM', 'jti'))
        if BlacklistedToken.objects.filter(token__jti=jti).exists():
            raise exceptions.AuthenticationFailed(InvalidMessage.BLACK_LIST)

        user = User.objects.filter(pk=validated_token.payload.get('user_id')).select_related('role').first()
        if not user or not user.status:
            raise InvalidUserId

        return user, validated_token
