from functools import wraps
from rest_framework.exceptions import PermissionDenied


def permit_if_role_in(allowed_roles=()):

    def view_wrapper_function(decorated_view_function):

        @wraps(decorated_view_function)
        def check_user_roles(view, request, *args, **kwargs):
            user = request.user
            print(user, "user122334")
            if not user or user.role.code not in allowed_roles:
                raise PermissionDenied

            response = decorated_view_function(view, request, *args, **kwargs)

            return response

        return check_user_roles

    return view_wrapper_function
