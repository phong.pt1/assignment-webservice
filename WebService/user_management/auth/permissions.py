from django.conf import settings
from rest_framework import permissions

from ..helpers.permissions import SYSTEM_ADMIN, VIEWER
from ..helpers.constants import API_KEY_HEADER


class IsViewer(permissions.BasePermission):
    def has_permission(self, request, view):
        user = request.user
        return bool(user and user.status and user.role.code == VIEWER)


class IsSystemAdmin(permissions.BasePermission):
    def has_permission(self, request, view):
        # return True
        user = request.user
        return bool(user and user.status and user.role.code == SYSTEM_ADMIN)


class HasAPIKey(permissions.BasePermission):
    def has_permission(self, request, view):
        api_key = request.headers.get(API_KEY_HEADER)
        return bool(api_key and api_key == settings.API_KEY)


class IsAuthenticatedByTokenOrKey(permissions.BasePermission):
    def has_permission(self, request, view):
        is_valid_token = bool(request.user and request.user.is_authenticated)
        api_key = request.headers.get(API_KEY_HEADER)
        is_valid_api_key = bool(api_key and api_key == settings.API_KEY)

        return is_valid_token or is_valid_api_key
