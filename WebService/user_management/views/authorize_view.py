from rest_framework.views import APIView
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated


class RoleVerifyView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        user, role = request.user, str(request.data.get('role'))
        if user.role.code != role:
            raise PermissionDenied()

        return Response({})


class PermissionVerifyView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request):
        role, permission = request.user.role, str(request.data.get('permission'))
        if not role.permissions.filter(code=permission).exists():
            raise PermissionDenied()

        return Response({})
