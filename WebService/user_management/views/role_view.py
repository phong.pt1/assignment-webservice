from rest_framework import viewsets

from ..models import Role
from ..serializers import ListRoleSerializer


class RoleViewSet(viewsets.ModelViewSet):
    serializer_class = ListRoleSerializer
    queryset = Role.objects.all()
    pagination_class = None
    http_method_names = ['get']
