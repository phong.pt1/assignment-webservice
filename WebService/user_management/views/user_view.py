import copy
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated

from common import drf
from ..models import User
from ..serializers import (
    CreateUserSerializer, UpdateUserSerializer, DetailUserSerializer, ListUserSerializer, ChangePasswordSerializer,
    ResetPasswordSerializer, DeleteUserSerializer,
)
from ..auth import IsSystemAdmin, HasAPIKey


class UserViewSet(viewsets.ModelViewSet):
    """
    - [GET]     - /api/users/                        - API get list user
    - [POST]    - /api/users/                        - API create user
    - [GET]     - /api/users/{id}/                   - API detail user by user_id
    - [PUT]     - /api/users/{id}/                   - API update user
    - [DELETE]  - /api/users/{id}/                   - API delete user
    - [PUT]     - /api/users/{id}/reset_password/    - API reset password by user_id
    - [PUT]     - /api/users/{id}/change_password/   - API change password by user_id
    - [PUT]     - /api/users/{id}/disable/           - API disable/enable user by user_id
    - [DELETE]  - /api/users/delete/                 - API delete multi user
    - [GET]     - /api/users/info/                   - API get user info
    - [PUT]     - /api/users/update_profile/         - API update current user
    - [PUT]     - /api/users/update_password/        - API update password for current user
    """

    def get_permissions(self):
        if self.action in ['info', 'update_profile', 'update_password']:
            self.permission_classes = [IsAuthenticated]
        elif self.action in ['list', 'retrieve']:
            self.permission_classes = [HasAPIKey | IsSystemAdmin]
        else:
            self.permission_classes = [IsSystemAdmin]

        return super().get_permissions()

    def get_serializer_class(self):
        if self.action == 'create':
            return CreateUserSerializer
        if self.action == 'update':
            return UpdateUserSerializer
        if self.action == 'list':
            return ListUserSerializer

        return DetailUserSerializer

    def __context(self):
        return {'request': self.request}

    def get_queryset(self):
        queryset = User.objects.all().select_related('role')

        search_key = self.request.query_params.get('search_key')
        if search_key:
            queryset = queryset.filter(full_name__icontains=search_key.strip())

        return queryset.order_by('-created_at')

    def list(self, request, *args, **kwargs):
        return drf.get_paginated_response(
            pagination_class=PageNumberPagination,
            serializer_class=self.get_serializer_class(),
            queryset=self.get_queryset(),
            request=request,
            view=self
        )

    @action(detail=True, methods=['put'])
    def reset_password(self, request, pk):
        serializer = ResetPasswordSerializer(data=request.data, instance=self.get_object(), context=self.__context())
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({}, status.HTTP_204_NO_CONTENT)

    @action(detail=True, methods=['put'])
    def change_password(self, request, pk):
        serializer = ChangePasswordSerializer(data=request.data, instance=self.get_object(), context=self.__context())
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({}, status.HTTP_204_NO_CONTENT)

    @action(detail=False, methods=['delete'])
    def delete(self, request):
        serializer = DeleteUserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        User.objects.filter(id__in=serializer.validated_data['users']).delete()
        return Response({}, status.HTTP_204_NO_CONTENT)

    @action(detail=True, methods=['put'])
    def disable(self, request, pk):
        user = self.get_object()
        user.status = False if user.status else True
        user.updated_by = request.user.id
        user.save()

        return Response(ListUserSerializer(user).data)

    @action(detail=False, methods=['get'])
    def info(self, request):
        return Response(DetailUserSerializer(request.user).data)

    @action(detail=False, methods=['put'])
    def update_profile(self, request):
        user = request.user
        data = copy.copy(request.data)
        data['role_id'] = user.role.id
        data['status'] = user.status
        data.pop('cases', None)
        data.pop('rules', None)
        update_serializer = UpdateUserSerializer(data=data, instance=user, context=self.__context())
        update_serializer.is_valid(raise_exception=True)
        user = update_serializer.save()
        return Response(DetailUserSerializer(user).data)

    @action(detail=False, methods=['put'])
    def update_password(self, request):
        serializer = ChangePasswordSerializer(data=request.data, instance=request.user, context=self.__context())
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response(DetailUserSerializer(user).data)
