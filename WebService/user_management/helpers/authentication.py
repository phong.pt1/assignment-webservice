class InvalidMessage:
    TOKEN_HEADER = 'Invalid token header.'
    TOKEN_HEADER_WITH_SPACE = 'Invalid token header. Token string should not contain spaces.'
    BLACK_LIST = 'Token is blacklisted'
    API_KEY = 'Invalid api key.'
    USER_INACTIVE = 'Your account is inactive, please contact admin to solve it'
