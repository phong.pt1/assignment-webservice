# ================= ROLE =======================
SYSTEM_ADMIN, VIEWER = 'system_admin', 'viewer'
ROLES = {
    SYSTEM_ADMIN: {
        'name': 'Admin',
    },
    VIEWER: {
        'name': 'Viewer',
    },
}
# ==============================================

# =============== PERMISSION ===================
# review case
PEM_VIEW_REVIEW_CASE = 'view_review_case'
PEM_ADD_REVIEW_CASE = 'add_review_case'
PEM_CHANGE_REVIEW_CASE = 'change_review_case'
PEM_DELETE_REVIEW_CASE = 'delete_review_case'
PEM_SHARE_REVIEW_CASE = 'share_review_case'

# rule model
PEM_VIEW_RULE = 'view_rule'
PEM_ADD_RULE = 'add_rule'
PEM_CHANGE_RULE = 'change_rule'
PEM_DELETE_RULE = 'delete_rule'
PEM_SHARE_RULE = 'share_rule'

# user management
PEM_VIEW_USER = 'view_user'
PEM_ADD_USER = 'add_user'
PEM_CHANGE_USER = 'change_user'
PEM_DELETE_USER = 'delete_user'
# =============================================


ROLE_PERMISSION = {
    SYSTEM_ADMIN: [
        PEM_VIEW_REVIEW_CASE, PEM_ADD_REVIEW_CASE, PEM_CHANGE_REVIEW_CASE, PEM_DELETE_REVIEW_CASE, PEM_SHARE_REVIEW_CASE,
        PEM_VIEW_RULE, PEM_ADD_RULE, PEM_CHANGE_RULE, PEM_DELETE_RULE, PEM_SHARE_RULE,
        PEM_VIEW_USER, PEM_ADD_USER, PEM_CHANGE_USER, PEM_DELETE_USER,
    ],
    VIEWER: [PEM_VIEW_REVIEW_CASE, PEM_VIEW_RULE, PEM_VIEW_USER],
}
