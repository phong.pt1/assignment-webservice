class InvalidMessage:
    USER_UNIQUE = 'Create user fail, user already existed, please check again!'
    NOT_FOUND = '{} with id={} not exist.'
    OLD_PASSWORD_MISMATCHED = 'Old password is not correct, please check again'
    NEW_PASSWORD_MATCHED = 'New password cannot be the same as the old password, please check again'
