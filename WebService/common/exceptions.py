from rest_framework.exceptions import APIException, NotFound, ValidationError


class ApplicationError(APIException):
    status_code = 500
    default_code = 'application_error'
    default_detail = 'Something went wrong on server'


class NotFoundError(NotFound):
    pass


class ServiceUnavailableError(ApplicationError):
    status_code = 503
    default_code = 'service_unavailable'
    default_detail = 'An internal service was unavailable'


class InvalidInputError(ValidationError):
    default_code = 'invalid'
    default_detail = 'Invalid input params'


class GroupNameAlreadyExist(ValidationError):
    default_code = 'group_name_exist'
    default_detail = 'Name of group already exists'
