from drf_standardized_errors.formatter import ExceptionFormatter
from drf_standardized_errors.types import ErrorResponse
from drf_standardized_errors.handler import ExceptionHandler
from django.core.exceptions import ValidationError as DjangoValidationError
from django.db.utils import IntegrityError as DjangoIntegrityError
from rest_framework.exceptions import ValidationError as DRFValidationError
from rest_framework.serializers import as_serializer_error

from common.exceptions import ApplicationError


class MyExceptionHandler(ExceptionHandler):
    def convert_known_exceptions(self, exc: Exception) -> Exception:
        if isinstance(exc, DjangoValidationError):
            return DRFValidationError(as_serializer_error(exc))
        elif isinstance(exc, DjangoIntegrityError):
            return ApplicationError({f'{str(exc)}': 'Database error'})
        else:
            return super().convert_known_exceptions(exc)


class MyExceptionFormatter(ExceptionFormatter):
    def format_error_response(self, error_response: ErrorResponse):
        error = error_response.errors[0]
        return {
            "type": error_response.type,
            "code": error.code,
            "message": error.detail,
            "extra": error.attr,
        }

