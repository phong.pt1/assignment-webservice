import logging
import bson
from collections import OrderedDict
from django.apps import apps
from django.utils import timezone
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework import serializers, exceptions
from rest_framework.pagination import LimitOffsetPagination as _LimitOffsetPagination
from rest_framework.response import Response
from rest_framework.utils.urls import replace_query_param

from common.base_model import BaseSimpleModel, BaseModel

logger = logging.getLogger('app')
UserClass = apps.get_model('user_management', 'User')


class ActorType:
    SYSTEM = -1
    EVENT_POST_PROCESSOR = -2
    CORE_API = -3
    SMART_DETECTOR = -4


def make_mock_object(**kwargs):
    return type("", (object,), kwargs)


def get_object(model_or_queryset, **kwargs):
    """
    Reuse get_object_or_404 since the implementation supports both Model && queryset.
    Catch Http404 & return None
    """
    try:
        return get_object_or_404(model_or_queryset, **kwargs)
    except Http404:
        return None



def get_paginated_response(*, pagination_class, serializer_class, queryset, request, view):
    paginator = pagination_class()

    page = paginator.paginate_queryset(queryset, request, view=view)

    if page is not None:
        serializer = serializer_class(page, many=True)
        return paginator.get_paginated_response(serializer.data)

    serializer = serializer_class(queryset, many=True)

    return Response(data=serializer.data)


class LimitOffsetPagination(_LimitOffsetPagination):
    default_limit = 10
    max_limit = 50

    def get_paginated_data(self, data):
        return OrderedDict(
            [
                ("limit", self.limit),
                ("offset", self.offset),
                ("count", self.count),
                ("next", self.get_next_link()),
                ("previous", self.get_previous_link()),
                ("results", data),
            ]
        )

    def get_paginated_response(self, data):
        """
        We redefine this method in order to return `limit` and `offset`.
        This is used by the frontend to construct the pagination itself.
        """
        return Response(
            OrderedDict(
                [
                    ("limit", self.limit),
                    ("offset", self.offset),
                    ("count", self.count),
                    ("next", self.get_next_link()),
                    ("previous", self.get_previous_link()),
                    ("results", data),
                ]
            )
        )


class FastPagination(_LimitOffsetPagination):
    default_limit = 10
    max_limit = 50
    limit = 10
    offset = 0
    is_end_page = False

    def paginate_queryset(self, queryset, request, view=None):  # pragma: no cover
        self.limit = self.get_limit(request)
        if self.limit is None:
            return None

        self.offset = self.get_offset(request)
        self.request = request  # noqa

        # get one extra row
        return list(queryset[self.offset:self.offset + self.limit + 1])

    def get_paginated_response(self, data):
        self.is_end_page = True
        if len(data) == self.limit + 1:
            data = data[:-1]
            self.is_end_page = False

        return Response(
            OrderedDict(
                [
                    ("limit", self.limit),
                    ("offset", self.offset),
                    ("next", self.get_next_link()),
                    ("previous", self.get_previous_link()),
                    ("results", data),
                ]
            )
        )

    def get_next_link(self):
        if self.is_end_page:
            return None

        url = self.request.build_absolute_uri()
        url = replace_query_param(url, self.limit_query_param, self.limit)

        offset = self.offset + self.limit
        return replace_query_param(url, self.offset_query_param, offset)


def model_create(model_class, validated_data, write_db=True):
    # set base model field
    if issubclass(model_class, BaseSimpleModel):
        user_id = validated_data.pop('user_id', None) or ActorType.SYSTEM
        now = timezone.now()
        if issubclass(model_class, BaseModel):
            actor = user_id
        else:
            actor = UserClass(id=user_id)

        validated_data['created_by'] = actor
        validated_data['updated_by'] = actor

        # model.save will auto set created_at, updated_at. This is a backup.
        validated_data['created_at'] = now
        validated_data['updated_at'] = now

    instance = model_class(**validated_data)

    print(instance, "instance")
    if getattr(instance, 'custom_clean', None):
        instance.custom_clean()  # noqa

    if write_db:
        instance.save()
    return instance


def model_update(instance, data, force_updated_fields=None):
    fields = []
    model_class = type(instance)
    if issubclass(model_class, BaseSimpleModel):
        # set base model field
        user_id = data.pop('user_id', None) or ActorType.SYSTEM
        now = timezone.now()
        if issubclass(model_class, BaseModel):
            actor = user_id
        else:
            actor = UserClass(id=user_id)

        setattr(instance, 'updated_by', actor)
        setattr(instance, 'updated_at', now)

        fields = ['updated_by', 'updated_at']

    # check which data fields are modified
    for field, value in data.items():
        logger.info('FIELDS: %s %s %s' % (field, value, getattr(instance, field)))
        if getattr(instance, field) != value:
            fields.append(field)
            setattr(instance, field, value)

    # Perform an update only if any of the fields was actually changed
    if force_updated_fields:
        fields += force_updated_fields

    if fields:
        if getattr(instance, 'custom_clean', None):
            instance.custom_clean(set(fields))
        # Update only the fields that are meant to be updated.
        logger.info('UPDATE FIELDS: %s %s' % (instance.id, fields))
        instance.save(update_fields=fields)

    return instance


class MyModelSerializer(serializers.ModelSerializer):
    def get_validators(self):
        return []

    def include_extra_kwargs(self, kwargs, extra_kwargs):
        super().include_extra_kwargs(kwargs, extra_kwargs)

        kwargs.pop('validators', None)
        return kwargs


# class ObjectIdField(serializers.CharField):
#     def to_representation(self, value):
#         return str(value)

#     def to_internal_value(self, data):
#         if not isinstance(data, str):
#             msg = 'Incorrect type. Expected a string, but got %s'
#             raise exceptions.ValidationError(msg % type(data).__name__)

#         try:
#             return bson.ObjectId(data)
#         except bson.errors.InvalidId:
#             raise exceptions.ValidationError('Object id is invalid')
