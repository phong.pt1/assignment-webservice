import logging
from common.exceptions import InvalidInputError
logger = logging.getLogger('api')
def parse_sort_params(params):
    try:
        fields = params.split(',')
        orders = []
        for field in fields:
            tokens = field.split(':')
            if tokens[1] == 'asc':
                orders.append(tokens[0])
            elif tokens[1] == 'desc':
                orders.append('-' + tokens[0])
        return orders
    except Exception as ex:
        logger.error(ex)
        raise InvalidInputError({'sort': 'Invalid sort params'})