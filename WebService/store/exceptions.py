from rest_framework.exceptions import ValidationError, NotFound


class StoreNotFound(NotFound):
    default_code = 'store_not_found'
    default_detail = 'Store not found'


class CameraGroupNotFound(NotFound):
    default_code = 'store_group_not_found'
    default_detail = 'Store group not found'


class ChargingStationNotFound(NotFound):
    default_code = 'charging_station_not_found'
    default_detail = 'Charging station not found'
