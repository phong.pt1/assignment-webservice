from django.urls import path
from .views import StoreView, ProductView, StoreProductView
app_name = 'store'

urlpatterns = [
    path('', StoreView.as_view()),
    path('product', ProductView.as_view()),
    path('store-product', StoreProductView.as_view()),

]