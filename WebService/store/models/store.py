from django.db import models
# from django.contrib.auth.models import User
from user_management.models.user import User
from .product import Product


# Create your models here.
class Store(models.Model):
    user = models.OneToOneField(User, on_delete=models.DO_NOTHING, null=False)
    name = models.CharField(max_length=255)
    desc = models.CharField(max_length=255)
    location = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    products = models.ManyToManyField(Product, through='StoreProduct')

    class Meta:
        managed = True
        db_table = 'store'

