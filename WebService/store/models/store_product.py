from django.db import models
from ..models import Product, Store


# Create your models here.
class StoreProduct(models.Model):
    store = models.ForeignKey(Store, on_delete=models.DO_NOTHING, related_name='storeproduct')
    product = models.ForeignKey(Product, on_delete=models.DO_NOTHING, related_name='storeproduct')
    quantity = models.IntegerField(null=False, default=0)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        managed = True
        db_table = 'store_products'

