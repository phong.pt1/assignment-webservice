from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from .serializers import StoreInputSerializer, StoreSimpleOutputSerializer, ProductInputSerializer, StoreProductOutputSerializer, ProductOutSerializer, StoreProductInputSerializer
from .store_service import create_store, get_list_store, remove_list_stores, create_product, create_storeproduct, get_product
from django.db.utils import IntegrityError
from rest_framework.response import Response
from rest_framework import serializers, status
from common.exceptions import ApplicationError
from user_management.auth import permit_if_role_in
from user_management.helpers.permissions import SYSTEM_ADMIN
from user_management.auth.permissions import IsSystemAdmin, HasAPIKey
from common import drf
from rest_framework.pagination import PageNumberPagination
import logging

logger = logging.getLogger('api')


class StoreView(APIView):

    # def get_permissions(self):
    #     self.permission_classes = [IsSystemAdmin]
    InputSerializer = StoreInputSerializer
    OutputSerializer = StoreSimpleOutputSerializer

    class FilterSerializer(serializers.Serializer):
        name = serializers.CharField(required=False)
        sort = serializers.CharField(required=False)

    class FilterDeleteSerializer(serializers.Serializer):
        stores = serializers.ListField(
            child=serializers.IntegerField(min_value=0)
        )

    def get(self, request):
        filter_serializer = self.FilterSerializer(data=request.query_params)
        filter_serializer.is_valid(raise_exception=True)
        validated_data = filter_serializer.validated_data

        stores = get_list_store(validated_data)

        if validated_data.get('all'):
            return Response({
                "results": self.OutputSerializer(stores, many=True).data
            })

        return drf.get_paginated_response(
            pagination_class=PageNumberPagination,
            serializer_class=self.OutputSerializer,
            queryset=stores,
            request=request,
            view=self
        )

    @permit_if_role_in([SYSTEM_ADMIN])
    def post(self, request):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        print(request.user, "data")
        try:
            store = create_store(validated_data, user=request.user)
        except IntegrityError as ex:
            raise ApplicationError({f'{str(ex)}': 'Database error'})
        return Response(self.OutputSerializer(store).data, status=status.HTTP_201_CREATED)

    @permit_if_role_in([SYSTEM_ADMIN])
    def delete(self, request):
        filter_serializer = self.FilterDeleteSerializer(data=request.data)
        filter_serializer.is_valid(raise_exception=True)
        validated_data = filter_serializer.validated_data
        list_store_ids = validated_data.get("stores")
        try:
            remove_list_stores(list_store_ids=list_store_ids)
            return Response({'message': 'OK'})
        except IntegrityError as ex:
            logger.exception(ex)
            raise ApplicationError({f'{str(ex)}': 'Failed to bulk delete cameras'})


class ProductView(APIView):
    InputSerializer = ProductInputSerializer
    OutputSerializer = ProductOutSerializer

    def get(self, request):
        product = get_product(user=request.user)
        return Response({
            "results": product
        })

    
    @permit_if_role_in([SYSTEM_ADMIN])
    def post(self, request):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        try:
            store = create_product(validated_data)
        except IntegrityError as ex:
            raise ApplicationError({f'{str(ex)}': 'Database error'})
        return Response(self.OutputSerializer(store).data, status=status.HTTP_201_CREATED)


class StoreProductView(APIView):
    InputSerializer = StoreProductInputSerializer
    OutputSerializer = StoreProductOutputSerializer
    @permit_if_role_in([SYSTEM_ADMIN])
    def post(self, request):
        serializer = self.InputSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        try:
            store = create_storeproduct(validated_data)
        except IntegrityError as ex:
            raise ApplicationError({f'{str(ex)}': 'Database error'})
        return Response(self.OutputSerializer(store).data, status=status.HTTP_201_CREATED)