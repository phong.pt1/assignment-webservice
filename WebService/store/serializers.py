import logging
from rest_framework import serializers
from .models.store import Store
from .models.product import Product
from .models.store_product import StoreProduct


class StoreInputSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=True)
    desc = serializers.CharField(required=False)
    location = serializers.CharField(required=False)
    user = serializers.IntegerField(required=False)


    class Meta:
        model = Store
        fields = '__all__'

class StoreSimpleOutputSerializer(serializers.ModelSerializer):

    class Meta:
        model = Store
        # fields = ['id', 'name', 'desc', 'location', 'user_id', 'created_at', 'updated_at']
        fields = '__all__'
        
class ProductInputSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=True)
    desc = serializers.CharField(required=False)
    price = serializers.IntegerField(required=False)

    class Meta:
        model = Product
        fields = '__all__'


class ProductOutSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class StoreProductInputSerializer(serializers.ModelSerializer):
    quantity = serializers.IntegerField(required=False)
    product_id = serializers.IntegerField(required=False)
    user_id = serializers.IntegerField(required=False)

    class Meta:
        model = StoreProduct
        fields = '__all__'
class StoreProductOutputSerializer(serializers.ModelSerializer):
    class Meta:
        model = StoreProduct
        fields = '__all__'