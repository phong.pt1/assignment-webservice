from django.db import transaction
from .models.store import Store
from .models.store import Product
from .models.store_product import StoreProduct
from django.utils import timezone
from common import drf
from utils import misc
from .exceptions import StoreNotFound
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q

@transaction.atomic
def create_store(validated_data: dict, user):
    validated_data['user_id'] = user.id
    store = drf.model_create(Store, validated_data)
    return store

@transaction.atomic
def create_product(validated_data: dict):
    product = drf.model_create(Product, validated_data)
    return product

@transaction.atomic
def create_storeproduct(validated_data: dict):
    quantity = validated_data.pop('quantity', [])
    instance = drf.model_create(StoreProduct, validated_data)
    instance.quantity = quantity
    instance.save()

    return instance


def get_list_store(validated_data):
    qs = Store.objects.all()
    # filter
    name = validated_data.get('name')
    if name:
        qs = qs.filter(name__icontains=name)

    list_store_ids = validated_data.get('cameras')
    if list_store_ids:
        qs = qs.filter(id__in=list_store_ids)

    # orders
    sort_params = validated_data.get('sort')
    orders = misc.parse_sort_params(sort_params) if sort_params else ['-created_at']

    return qs.order_by(*orders)

@transaction.atomic
def remove_store(store_id):
    store = get_store_for_update(store_id=store_id)
    print(store, 'list_store_ids')
    store.delete()

@transaction.atomic
def remove_list_stores(list_store_ids):
    for store_id in list_store_ids:
        remove_store(store_id)



def get_store_for_update(store_id):
    try:

        return Store.objects.select_for_update().get(pk=store_id)
    except ObjectDoesNotExist:
        raise StoreNotFound()



def get_product(user):
    query = '''
              select product.id, product.name
              from product
              join store_products sp
              on product.id = sp.product_id
              join store s
              on sp.store_id = s.id
              where s.user_id != %s
    
          ''' % (user.id)
    raw_results = Product.objects.raw(query)
    data = [{'product_id': o.id, 'name': o.name}
            for o in raw_results]
    # data = Product.objects.all().select_related('storeproduct').filter(~Q(id=user.id))
    # data = Product.objects.all().prefetch_related('store_products', 'store').filter(~Q(id=user.id))
    return data
